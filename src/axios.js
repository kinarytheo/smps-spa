import axios from "axios";
import router from "@/router"
import { useCookies } from "@vueuse/integrations/useCookies" 

const cookies = useCookies()
const baseUrl = `${import.meta.env.VITE_API_BASE_URL}/api`

const axiosClient = axios.create({
  baseURL: baseUrl,
  withCredentials: true
})

axiosClient.defaults.headers.common['Accept'] = 'application/json';
axiosClient.defaults.headers.common['Content-Type'] = 'application/json';

axiosClient.interceptors.request.use(config => {
  config.headers.Authorization = `Bearer ${cookies.get('simpus_token')}`
  return config;
})

axiosClient.interceptors.response.use(
  response => {
    return Promise.resolve(response);
  }, 
  error => {
    if ([401].includes(error.response.status)) {
      try {
        cookies.remove('simpus_token', {path: '/'})
      } catch (error) {
        console.log(error)
      }
      router.replace({name: 'login'})
    } else if ([404,403].includes(error.response.status)) {
      router.push({name: 'notfound'})
    }

    return Promise.reject(error)
  }
)

export default axiosClient;