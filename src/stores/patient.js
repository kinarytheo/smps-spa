import { defineStore } from 'pinia'

const urlPath = '/patients'

export const usePatientStore = defineStore('patientStore', {
  state: () => ({
    dataLists: [],
    dataMeta: [],
    dataLinks: {},
    search: '',
    filters: {
      patienttype: '',
      gender: 0
    },
    page: 1,
    loading: false,
  }),
  getters: {
    hasPaging: (state) => state.dataMeta.total > state.dataMeta.per_page,
    pagingCountInfo: (state) => {
      return `${state.dataMeta.from ?? 0} - ${state.dataMeta.to ?? 0} of ${state.dataMeta.total}`
    }
  },
  actions: {
    async fetchData (limit) {
      this.dataLists = { loading: true }
      await this.http.get(`${urlPath}`, { 
        params: {
          ...(limit ? { 'limit': limit } : {}),
          ...(this.search.length > 0 ? { 'q': this.search } : {}),
        }  
      })
        .then(response => {
          this.dataLists = response.data.data
          this.dataMeta = response.data.meta
          this.dataLinks = response.data.links
        })
        .catch(error => {})
    },

    async find (id) {
      return await this.http.get(`${urlPath}/${id}`)
    },

    async create (values) {
      return await this.http.post(`${urlPath}`, values)
    },

    async update (id, values) {
      return await this.http.put(`${urlPath}/${id}`, values)
    },

    async delete (id) {
      return await this.http.delete(`${urlPath}/${id}`)
    },

    async loadMore () {
      this.loading = true
      await this.http.get(`${urlPath}`, { 
        params: {
          page: this.page + 1,
          ...(this.search.length > 0 ? { 'q': this.search } : {}),
        }
      }).then(response => {
        this.dataLists.push(...response.data.data)
        this.dataMeta = response.data.meta
        this.dataLinks = response.data.links
        this.loading = false
      }).catch(error => {})

      this.page = this.page + 1
    }

  }
})