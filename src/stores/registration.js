import { defineStore } from 'pinia'
import { useNow, useDateFormat } from "@vueuse/core"
import { useCalculateAge } from './calculateAge'

export const useRegistrationStore = defineStore('registrationStore', {
  state: () => ({
    dataReference: [],
    visitType: [
      {id: 1, value: 'Lama'},
      {id: 2, value: 'Baru'},
      {id: 3, value: 'Kunjungan Kasus'},
    ],
    regionRef: {
      origin: {
        regencies: [],
        districts: [],
        villages: [],
        hamlets: []
      },
      domisili: {
        regencies: [],
        districts: [],
        villages: [],
        hamlets: []
      }
    },
    formData: {
      tgl: useDateFormat(useNow(), "DD-MM-YYYY", { locales: "id-ID" }).value,
      entry_id: null,
      facility_id: "",
      type_kunj: 1,
      department_id: [1],
      kunj_sakit: 1,
      patienttype_id: 7,
      no_rm: "",
      nik: "",
      no_kartu: "",
      no_bpjs: "",
      nama_kk: "",
      nama: "",
      no_hp: "",
      tempat_lahir: "",
      tgl_lahir: "",
      umur: "",
      bulanlahir: "",
      harilahir: "",
      groupvisit: 1,
      sex_id: "",
      bloodtype_id: "",
      job_id: "",
      no_bpjs: "",
      alamat: "",
      administrasi_pasien: "0101",
      education_id: "",
      village_id: "",
      hamlet_id: "",
      bpjs_poli: "",
      ppk_cocok: true,
      status_bpjs: "OK",
      provider: "OK",
    },
    errors: [],
    originAsDomisili: false,
    originAddress: {
      alamat: "",
      rt: "",
      rw: "",
      province_id: "",
      regency_id: "",
      district_id: "",
      village_id: "",
      hamlet_id: "",
    },
    domisiliAddress: {
      alamat: "",
      rt: "",
      rw: "",
      province_id: "",
      regency_id: "",
      district_id: "",
      village_id: "",
      hamlet_id: "",
    },
  }),
  getters: {
    bpjsPoly: (state) => (sakit) => {
      const data = Object.assign([], state.dataReference.bpjs_departments) 
      if (data == undefined) { return [] }
      return data.filter((item) => item.poliSakit == sakit)
    },
    facilities: (state) => {
      const data = Object.assign([], state.dataReference.facilities) 
      return data.map((item) => {
        return {
          id: item.id,
          kode: item.kode,
          value: `${item.kode} - ${item.faskes}`,
          label: `${item.kode} - ${item.faskes}`
        }
      })
    }, 
    patienttypes: (state) => {
      const data = Object.assign([], state.dataReference.patienttypes) 
      return data.map((item) => {
        return {
          id: item.id,
          kode: item.kode,
          value: `${item.kode} - ${item.jenis_pasien}`,
          label: `${item.kode} - ${item.jenis_pasien}`
        }
      })
    }, 
    sexes: (state) => {
      const data = Object.assign([], state.dataReference.sexes) 
      return data.map((item) => {
        return {
          id: item.id,
          kode: item.kode,
          value: `${item.kode} - ${item.jenis_kelamin}`,
          label: `${item.kode} - ${item.jenis_kelamin}`
        }
      })
    }, 
    bloodtypes: (state) => {
      const data = Object.assign([], state.dataReference.bloodtypes) 
      return data.map((item) => {
        return {
          id: item.id,
          kode: item.kode,
          value: `${item.kode} - ${item.golongan_darah}`,
          label: `${item.kode} - ${item.golongan_darah}`
        }
      })
    }, 
    educations: (state) => {
      const data = Object.assign([], state.dataReference.educations) 
      return data.map((item) => {
        return {
          id: item.id,
          kode: item.kode,
          value: `${item.kode} - ${item.pendidikan}`,
          label: `${item.kode} - ${item.pendidikan}`
        }
      })
    }, 
    jobs: (state) => {
      const data = Object.assign([], state.dataReference.jobs) 
      return data.map((item) => {
        return {
          id: item.id,
          kode: item.kode,
          value: `${item.kode} - ${item.pekerjaan}`,
          label: `${item.kode} - ${item.pekerjaan}`
        }
      })
    }, 
    departments: (state) => state.dataReference.departments, 
    tribes: (state) => {
      const data = Object.assign([], state.dataReference.tribes) 
      return data.map((item) => {
        return {
          id: item.id,
          kode: item.kode,
          value: `${item.kode} - ${item.suku}`,
          label: `${item.kode} - ${item.suku}`
        }
      })
    }, 
    provinces: (state) => state.dataReference.provinces, 
    kecacatan: (state) => {
      const data = state.dataReference.kecacatan
      if (data !== undefined) {
        return data.penyandang_cacat
      }
    },
  },
  actions: {
    formatDate(date) {
			return useDateFormat(date, "DD-MM-YYYY", { locales: "id-ID" }).value;
		},
    setAge(dateString) {
      const calculateAge = useCalculateAge()
      const valDate = calculateAge.originDateValue(dateString);
      const isValid = calculateAge.isDateValid(valDate);
      const age = calculateAge.getAge(dateString);
      if (isValid && age?.year >= 0) {
        this.formData.umur = age?.year;
        this.formData.bulanlahir = age?.month;
        this.formData.harilahir = age?.day;
      } else {
        this.formData.umur = "";
        this.formData.bulanlahir = "";
        this.formData.harilahir = "";
      }
    },
    fetchDataReference () {
      if (this.dataReference.length == 0) {
        this.http.get('/registrations/referensi')
          .then(response => {
            this.dataReference = response.data.data
          })
      }
    },
    async registrationStore (data) {
      return await new Promise((resolve, reject) => {
         this.http.post('/registrations', data)
          .then(response => { resolve(response) })
          .catch(error => { reject(error) })
      })
    },
    async fetchRegencies (province_id) {
      return await new Promise((resolve, reject) => {
        this.http.get(`/provinces/${province_id}/regencies`)
          .then(response => { return resolve(response) })
          .catch(error => { return reject(error) })
      })
    },
    async fetchDistricts (regency_id) {
      return await new Promise((resolve, reject) => {
        this.http.get(`/regencies/${regency_id}/districts`)
          .then(response => { return resolve(response) })
          .catch(error => { return reject(error) })
      })
    },
    async fetchVillages (district_id) {
      return await new Promise((resolve, reject) => {
        this.http.get(`/districts/${district_id}/villages`)
          .then(response => { return resolve(response) })
          .catch(error => { return reject(error) })
      })
    },
    async fetchhamlets (village_id) {
      return await new Promise((resolve, reject) => {
        this.http.get(`/villages/${village_id}/hamlets`)
          .then(response => { return resolve(response) })
          .catch(error => { return reject(error) })
      })
    },
    register() {
			let departments = this.formData.department_id;
			const requestData = departments.map((item) => {
				return {
					...this.formData,
					department_id: item,
				};
			});
			requestData.forEach((attribute) => {
				this.registrationStore(attribute)
					.then((response) => {
						console.log(response);
						this.resetField();
					})
					.catch((error) => {
						if (error.response) {
							const errorData = error.response.data;
							if (error.response.status == 422) {
								this.errors = errorData.errors;
								window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
							}
						}
					});
			});
		},
    resetField() {
			this.errors = [];
      this.formData.tgl = useDateFormat(useNow(), "DD-MM-YYYY", { locales: "id-ID" }).value
      this.formData.facility_id = ""
      this.formData.type_kunj = 1
      this.formData.department_id = [1]
      this.formData.kunj_sakit = 1
      this.formData.patienttype_id = ""
      this.formData.no_rm = ""
      this.formData.nik = ""
      this.formData.no_kartu = ""
      this.formData.no_bpjs = ""
      this.formData.nama_kk = ""
      this.formData.nama = ""
      this.formData.no_hp = ""
      this.formData.tempat_lahir = ""
      this.formData.tgl_lahir = ""
      this.formData.umur = ""
      this.formData.bulanlahir = ""
      this.formData.harilahir = ""
      this.formData.groupvisit = 1
      this.formData.sex_id = ""
      this.formData.bloodtype_id = ""
      this.formData.job_id = ""
      this.formData.no_bpjs = ""
      this.formData.alamat = ""
      this.formData.administrasi_pasien = "0101"
      this.formData.education_id = ""
      this.formData.village_id = ""
      this.formData.hamlet_id = ""
      this.formData.bpjs_poli = ""
      this.formData.ppk_cocok = true
      this.formData.status_bpjs = "OK"
      this.formData.provider = "OK"

			this.originAddress = {
				alamat: "",
				rt: "",
				rw: "",
				province_id: "",
				regency_id: "",
				district_id: "",
				village_id: "",
				hamlet_id: "",
			};

			this.domisiliAddress = {
				alamat: "",
				rt: "",
				rw: "",
				province_id: "",
				regency_id: "",
				district_id: "",
				village_id: "",
				hamlet_id: "",
			};

			this.regionRef = {
				origin: {
					regencies: [],
					district: [],
					villages: [],
					hamlets: [],
				},
				domisili: {
					regencies: [],
					district: [],
					villages: [],
					hamlets: [],
				},
			};
		},
  }
})