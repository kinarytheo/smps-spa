import { defineStore } from 'pinia'

export const useRegionStore = defineStore('regionStore', {
  state: () => ({
    provinces: [],
    regencies: [],
    districts: [],
    villages: [],
    hamlets: []
  }),
  getters: {
    provinceOptionLists: (state) => {
      return Object.assign([], state.provinces)
        .map(item => { 
          return {
            value: item.id,
            text: item.provinsi
          }
        })
    },

    regencyOptionLists: (state) => {
      return Object.assign([], state.regencies)
        .map(item => { 
          return {
            value: item.id,
            text: item.kabupaten
          }
        })
    },

    districtOptionLists: (state) => {
      return Object.assign([], state.districts)
        .map(item => { 
          return {
            value: item.id,
            text: item.kecamatan
          }
        })
    },

    villageOptionLists: (state) => {
      return Object.assign([], state.villages)
        .map(item => { 
          return {
            value: item.id,
            text: item.desa
          }
        })
    },

    hamletOptionLists: (state) => {
      return Object.assign([], state.hamlets)
        .map(item => { 
          return {
            value: item.id,
            text: item.dusun
          }
        })
    },

  },
  actions: {
    fetchProvinces () {
      if (this.provinces.length == 0) {
        this.http.get('/provinces?limit=40')
          .then(response => {
            this.provinces = response.data.data
          })
      }
    },

    fetchRegencies (province_id) {
      this.http.get(`/provinces/${province_id}/regencies`)
        .then(response => {
          this.regencies = response.data.data
        })
  },

    fetchDistricts (regency_id) {
      this.http.get(`/regencies/${regency_id}/districts`)
        .then(response => {
          this.districts = response.data.data
        })
    },

    fetchVillages (district_id) {
      this.http.get(`/districts/${district_id}/villages`)
        .then(response => {
          this.villages = response.data.data
        })
    },

    fetchHamlets (village_id) {
      this.http.get(`/villages/${village_id}/hamlets`)
        .then(response => {
          this.hamlets = response.data.data
        })
  },
  }
})