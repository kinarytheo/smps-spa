import { defineStore } from 'pinia'
import { useRoute } from 'vue-router'

export const useMenuMasterStore = defineStore('menuMaster', {
  state: () => ({
    search: '',
    keyword: 'this is keyword',
    menulists: [
      { name: 'Penyakit', routeName: 'disease', path: '/master/penyakit', permission: 'disease_viewAny' },
      { name: 'Kelompok Penyakit', routeName: 'diseasegroup', path: '/master/kelompok-penyakit', permission: 'diseasegroup_viewAny' },
      { name: 'Obat', routeName: 'drug', path: '/master/obat', permission: 'drug_viewAny' },
      { name: 'Unit Pelayanan', routeName: 'department', path: '/master/unit-pelayanan', permission: 'department_viewAny' },
      { name: 'Tempat Periksa', routeName: 'facility', path: '/master/tempat-periksa', permission: 'facility_viewAny' },
      { name: 'Rumah Sakit', routeName: 'hospital', path: '/master/rumah-sakit', permission: 'hospital_viewAny' },
      { name: 'Poli Rujukan', routeName: 'hospitaldepartment', path: '/master/poli-rujukan', permission: 'hospitaldepartment_viewAny' },
      { name: 'Jenis Pasien', routeName: 'patienttype', path: '/master/jenis-pasien', permission: 'patienttype_viewAny' },
      { name: 'Tindak Lanjut', routeName: 'followup', path: '/master/tindak-lanjut', permission: 'followup_viewAny' },
      { name: 'Kelompok Umur', routeName: 'agegroup', path: '/master/kelompok-umur', permission: 'agegroup_viewAny' },
      { name: 'Golongan Darah', routeName: 'bloodtype', path: '/master/golongan-darah', permission: 'bloodtype_viewAny' },
      { name: 'Pendidikan', routeName: 'education', path: '/master/pendidikan', permission: 'education_viewAny' },
      { name: 'Pekerjaan', routeName: 'job', path: '/master/pekerjaan', permission: 'job_viewAny' },
      { name: 'Suku', routeName: 'tribe', path: '/master/suku', permission: 'tribe_viewAny' },
      { name: 'Desa', routeName: 'village', path: '/master/desa', permission: 'village_viewAny' },
      { name: 'Dusun', routeName: 'hamlet', path: '/master/dusun', permission: 'hamlet_viewAny' },
      { name: 'Item Lab', routeName: 'labtest', path: '/master/lab-item', permission: 'labtest_viewAny' },
      { name: 'Kelompok Lab', routeName: 'labtestgroup', path: '/master/lab-kelompok', permission: 'labtestgroup_viewAny' },
      { name: 'Tarif', routeName: 'tariff', path: '/master/tarif', permission: 'tariff_viewAny' },
      { name: 'Kelompok Tarif', routeName: 'tariffgroup', path: '/master/kelompok-tarif', permission: 'tariffgroup_viewAny' },
      { name: 'Kategori Tarif', routeName: 'tariffcategory', path: '/master/kategori-tarif', permission: 'tariffcategory_viewAny' },
      { name: 'Sumber Dana', routeName: 'fund', path: '/master/sumber-dana', permission: 'fund_viewAny' },
    ]
  }),
  getters: {
    filteredMenuList: (state) => {
      return state.menulists.filter(menu => {
        return menu.name.toLowerCase().includes(state.search.toLowerCase())
      })
    }
  },
  actions: {
    subIsActive (input) {
      const route = useRoute()
      const paths = Array.isArray(input) ? input : [input]
      return paths.some((path) => { 
        return route.matched[1].path === path 
      })
    }
  }
})