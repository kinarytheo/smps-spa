import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

const urlPath = '/hospitals'

export const useHospitalStore = defineStore('hospital', {
  state: () => ({
    dataLists: [],
    dataMeta: [],
    dataLinks: {},
    search: '',
  }),
  getters: {
    hasPaging: (state) => state.dataMeta.total > state.dataMeta.per_page,
    pagingCountInfo: (state) => {
      return `${state.dataMeta.from ?? 0} - ${state.dataMeta.to ?? 0} of ${state.dataMeta.total}`
    }
  },
  actions: {
    async fetchData () {
      this.dataLists = { loading: true }
      await this.http.get(`${urlPath}`, { 
        params: {
          ...(this.search.length > 0 ? { 'q': this.search } : {}),
        }  
      })
        .then(response => {
          this.dataLists = response.data.data
          this.dataMeta = response.data.meta
          this.dataLinks = response.data.links
        })
        .catch(error => {})
    },

    async find (id) {
      return await this.http.get(`${urlPath}/${id}`)
    },

    async create (values) {
      return await this.http.post(`${urlPath}`, values)
    },

    async update (id, values) {
      return await this.http.put(`${urlPath}/${id}`, values)
    },

    async delete (id) {
      return await this.http.delete(`${urlPath}/${id}`)
    },

    async pagePrev () {
      if (this.dataLinks.prev) {
        await this.http.get(this.dataLinks.prev).then(response => {
          this.dataLists = response.data.data
          this.dataMeta = response.data.meta
          this.dataLinks = response.data.links
        })
      }
    },

    async pageNext () {
      if (this.dataLinks.next) {
        await this.http.get(this.dataLinks.next).then(response => {
          this.dataLists = response.data.data
          this.dataMeta = response.data.meta
          this.dataLinks = response.data.links
        })
      }
    },
  }
})
