import { defineStore } from 'pinia'
import { useCookies } from '@vueuse/integrations/useCookies'

export const useAuthStore = defineStore('authStore', {
  state: () => ({
    username: '',
    password: '',
    user: {},
    loading: false,
    errorMessage: false,
    permissions: []
  }),
  getters: {
    getNameOfUser: (state) => state.user.name,
    getUsername: (state) => state.user.username,
    getUserEmail: (state) => state.user.email,
    getUserId: (state) => state.user.id
  },
  actions: {
    async login() {
      const cookies = useCookies()
      this.loading = true
      await this.http.post('/login', { username: this.username, password: this.password })
        .then(res => {
          if (res) {
            const token = res.data.data.token
            this.user = res.data.data.user
            cookies.set('simpus_token', token, {path: '/', sameSite: 'Strict'})
            this.errorMessage = false
            this.username = ''
            this.password = ''
            this.fetchPermissions()
          }
        })
        .catch(err => {
          if (err) {
            this.errorMessage = true
            this.loading = false
          }
        })

      this.loading = false
      this.router.push({ name: 'dashboard' })
    },

    async logout () {
      const cookies = useCookies()
      await this.http.post('/logout')
        .then(response => {
          if (response.status === 200) {
            try {
              cookies.remove('simpus_token')
            } catch (error) {}

            this.router.replace({ name: 'login' })
          }
        })
        .catch(error => {})
        
    },

    async fetchUser () {
      await this.http.get('/auth/me').then(response => {
        this.user = response.data.data
      })
    },

    async fetchPermissions () {
      await this.http.get('/auth/permissions').then(response => {
        this.permissions = response.data.data
      })
    },

    async authUserInfo() {
      try {
        await Promise.all([
          this.fetchUser(), this.fetchPermissions()
        ])
      } catch (error) {}
    },

    can (permission) {
      const controlList = Array.isArray(permission) ? permission : [permission]
      return controlList.some((item) => {
        return this.permissions.includes(item)
      })
    },
  },
})
