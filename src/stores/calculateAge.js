import { defineStore } from 'pinia'

export const useCalculateAge = defineStore('calculateAgeStore', () => {
  function isDateValid (datestr) {
    return !isNaN(new Date(datestr))
  }

  function originDateValue(datestr) {
    const valDate = datestr
    const day = valDate.substring(0,2)
    const month = valDate.substring(3,5)
    const year = valDate.substring(6,10)
  
    return `${year}-${month}-${day}`
  }

  function getAge(dateString) {
    let now = new Date()
    let yearNow = now.getFullYear()
    let monthNow = now.getMonth()
    let dateNow = now.getDate()
    let daysInMonth = new Date(yearNow, monthNow+1, 0).getDate()

    let dob = new Date(
        dateString.substring(6,10),
        dateString.substring(3,5)-1,                  
        dateString.substring(0,2)                   
    )
    
    let yearDob = dob.getFullYear()
    let monthDob = dob.getMonth()
    let dateDob = dob.getDate()
    
    let yearAge = yearNow - yearDob
    let monthAge = 0
    let dateAge = null

    if (monthNow >= monthDob) {
      monthAge = (monthNow - monthDob)
    } else {
        yearAge--
        monthAge = 12 + monthNow - monthDob
    }

    if (dateNow >= dateDob) {
        dateAge = dateNow - dateDob      
    } else {
        if (monthAge !== 0) { monthAge-- }
        dateAge = daysInMonth + dateNow - dateDob
    }

    let age =  {
        year: yearAge,
        month: monthAge,
        day: dateAge
    }

    return age
  }

  return { getAge, isDateValid, originDateValue }
})
