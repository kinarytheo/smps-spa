import MasterRootView from '@/views/master/MasterRootView.vue'
import { MasterView } from '@/views/master'
import { DiseaseIndex, DiseaseCreate, DiseaseEdit } from "@/views/master/disease"
import { DiseasegroupIndex, DiseasegroupCreate, DiseasegroupEdit } from '@/views/master/diseasegroup'
import { DrugIndex, DrugCreate, DrugEdit, DrugShow } from '@/views/master/drug'
import { DepartmentIndex, DepartmentCreate, DepartmentEdit } from '@/views/master/department'
import { FacilityIndex, FacilityCreate, FacilityEdit } from '@/views/master/facility'
import { HospitalIndex, HospitalCreate, HospitalEdit } from '@/views/master/hospital'
import { HospitaldepartmentIndex, HospitaldepartmentCreate, HospitaldepartmentEdit } from '@/views/master/hospitaldepartment'
import { PatienttypeIndex, PatienttypeCreate, PatienttypeEdit } from '@/views/master/patienttype'
import { FollowupIndex, FollowupCreate, FollowupEdit } from '@/views/master/followup'
import { AgegroupIndex, AgegroupCreate, AgegroupEdit } from '@/views/master/agegroup'
import { BloodtypeIndex, BloodtypeCreate, BloodtypeEdit } from '@/views/master/bloodtype'
import { EducationIndex, EducationCreate, EducationEdit } from '@/views/master/education' 
import { JobIndex, JobCreate, JobEdit } from '@/views/master/job'
import { TribeIndex, TribeCreate, TribeEdit } from '@/views/master/tribe'
import { VillageIndex, VillageCreate, VillageEdit } from '@/views/master/village'
import { HamletIndex, HamletCreate, HamletEdit } from '@/views/master/hamlet'
import { LabtestIndex, LabtestCreate, LabtestEdit } from '@/views/master/labtest'
import { LabtestgroupIndex, LabtestgroupCreate, LabtestgroupEdit } from '@/views/master/labtestgroup'
import { TariffIndex, TariffCreate, TariffEdit } from '@/views/master/tariff'
import { TariffcategoryIndex, TariffcategoryCreate, TariffcategoryEdit } from '@/views/master/tariffcategory'
import { TariffgroupIndex, TariffgroupCreate,  TariffgroupEdit } from '@/views/master/tariffgroup'
import { FundIndex, FundCreate, FundEdit } from '@/views/master/fund'

export default [
  {
    path: '/master',
    component: MasterRootView,
    meta: { requiresAuth: true, layout: 'LayoutApp' },
    children: [
      {
        path: '',
        name: 'master',
        component: MasterView
      },
      {
        path: 'penyakit',
        meta: { requiresAuth: true },
        children: [
          {
            path: '',
            name: 'disease',
            component: DiseaseIndex,
          },
          {
            path: 'create',
            name: 'disease:create',
            component: DiseaseCreate,
            meta: { requiresAuth: true },
          },
          {
            path: ':id/edit',
            name: 'disease:edit',
            component: DiseaseEdit,
            meta: { requiresAuth: true },
          },
          
        ]
      },

      {
        path: 'kelompok-penyakit',
        meta: { requiresAuth: true },
        children: [
          {
            path: '',
            name: 'diseasegroup',
            component: DiseasegroupIndex,
          },
          {
            path: 'create',
            name: 'diseasegroup:create',
            component: DiseasegroupCreate,
            meta: { requiresAuth: true },
          },
          {
            path: ':id/edit',
            name: 'diseasegroup:edit',
            component: DiseasegroupEdit,
            meta: { requiresAuth: true },
          },
        ]
      },

      {
        path: 'obat',
        meta: { requiresAuth: true },
        children: [
          {
            path: '',
            name: 'drug',
            component: DrugIndex,
          },
          {
            path: ':id/edit',
            name: 'drug:edit',
            component: DrugEdit,
            meta: { requiresAuth: true },
          },
          {
            path: ':id',
            name: 'drug:show',
            component: DrugShow,
            meta: { requiresAuth: true },
          },
          {
            path: 'create',
            name: 'drug:create',
            component: DrugCreate,
            meta: { requiresAuth: true },
          },
        ]
      },
      
      // {
      //   path: 'petugas',
      //   meta: { requiresAuth: true },
      //   children: [
      //     {
      //       path: '',
      //       name: 'staff',
      //       component: StaffIndex,
      //     },
      //     {
      //       path: 'create',
      //       name: 'staff:create',
      //       component: StaffCreate,
      //       meta: { requiresAuth: true },
      //     },
      //     {
      //       path: ':id/edit',
      //       name: 'staff:edit',
      //       component: StaffEdit,
      //       meta: { requiresAuth: true },
      //     },
      //   ]
      // },

      {
        path: 'unit-pelayanan',
        meta: { requiresAuth: true },
        children: [
          {
            path: '',
            name: 'department',
            component: DepartmentIndex,
          },
          {
            path: ':id/edit',
            name: 'department:edit',
            component: DepartmentEdit,
            meta: { requiresAuth: true },
          },
          {
            path: 'create',
            name: 'department:create',
            component: DepartmentCreate,
            meta: { requiresAuth: true },
          },
        ]
      },
      
      {
        path: 'tempat-periksa',
        meta: { requiresAuth: true },
        children: [
          {
            path: '',
            name: 'facility',
            component: FacilityIndex,
          },
          {
            path: ':id/edit',
            name: 'facility:edit',
            component: FacilityEdit,
            meta: { requiresAuth: true },
          },
          {
            path: 'create',
            name: 'facility:create',
            component: FacilityCreate,
            meta: { requiresAuth: true },
          },
        ]
      },

      {
        path: 'rumah-sakit',
        meta: { requiresAuth: true },
        children: [
          {
            path: '',
            name: 'hospital',
            component: HospitalIndex,
          },
          {
            path: ':id/edit',
            name: 'hospital:edit',
            component: HospitalEdit,
            meta: { requiresAuth: true },
          },
          {
            path: 'create',
            name: 'hospital:create',
            component: HospitalCreate,
            meta: { requiresAuth: true },
          },
        ]
      },

      {
        path: 'poli-rujukan',
        meta: { requiresAuth: true },
        children: [
          {
            path: '',
            name: 'hospitaldepartment',
            component: HospitaldepartmentIndex,
          },
          {
            path: ':id/edit',
            name: 'hospitaldepartment:edit',
            component: HospitaldepartmentEdit,
            meta: { requiresAuth: true },
          },
          {
            path: 'create',
            name: 'hospitaldepartment:create',
            component: HospitaldepartmentCreate,
            meta: { requiresAuth: true },
          },
        ]
      },
      
      {
        path: 'jenis-pasien',
        meta: { requiresAuth: true },
        children: [
          {
            path: '',
            name: 'patienttype',
            component: PatienttypeIndex,
          },
          {
            path: ':id/edit',
            name: 'patienttype:edit',
            component: PatienttypeEdit,
            meta: { requiresAuth: true },
          },
          {
            path: 'create',
            name: 'patienttype:create',
            component: PatienttypeCreate,
            meta: { requiresAuth: true },
          },
        ]
      },
      
      {
        path: 'tindak-lanjut',
        meta: { requiresAuth: true },
        children: [
          {
            path: '',
            name: 'followup',
            component: FollowupIndex,
          },
          {
            path: ':id/edit',
            name: 'followup:edit',
            component: FollowupEdit,
            meta: { requiresAuth: true },
          },
          {
            path: 'create',
            name: 'followup:create',
            component: FollowupCreate,
            meta: { requiresAuth: true },
          },
        ]
      },

      {
        path: 'kelompok-umur',
        meta: { requiresAuth: true },
        children: [
          {
            path: '',
            name: 'agegroup',
            component: AgegroupIndex,
          },
          {
            path: ':id/edit',
            name: 'agegroup:edit',
            component: AgegroupEdit,
            meta: { requiresAuth: true },
          },
          {
            path: 'create',
            name: 'agegroup:create',
            component: AgegroupCreate,
            meta: { requiresAuth: true },
          },
        ]
      },

      {
        path: 'golongan-darah',
        meta: { requiresAuth: true },
        children: [
          {
            path: '',
            name: 'bloodtype',
            component: BloodtypeIndex,
          },
          {
            path: ':id/edit',
            name: 'bloodtype:edit',
            component: BloodtypeEdit,
            meta: { requiresAuth: true },
          },
          {
            path: 'create',
            name: 'bloodtype:create',
            component: BloodtypeCreate,
            meta: { requiresAuth: true },
          },
        ]
      },

      {
        path: 'pendidikan',
        meta: { requiresAuth: true },
        children: [
          {
            path: '',
            name: 'education',
            component: EducationIndex,
          },
          {
            path: ':id/edit',
            name: 'education:edit',
            component: EducationEdit,
            meta: { requiresAuth: true },
          },
          {
            path: 'create',
            name: 'education:create',
            component: EducationCreate,
            meta: { requiresAuth: true },
          },
        ]
      },

      {
        path: 'pekerjaan',
        meta: { requiresAuth: true },
        children: [
          {
            path: '',
            name: 'job',
            component: JobIndex,
          },
          {
            path: ':id/edit',
            name: 'job:edit',
            component: JobEdit,
            meta: { requiresAuth: true },
          },
          {
            path: 'create',
            name: 'job:create',
            component: JobCreate,
            meta: { requiresAuth: true },
          },
        ]
      },

      {
        path: 'suku',
        meta: { requiresAuth: true },
        children: [
          {
            path: '',
            name: 'tribe',
            component: TribeIndex,
          },
          {
            path: ':id/edit',
            name: 'tribe:edit',
            component: TribeEdit,
            meta: { requiresAuth: true },
          },
          {
            path: 'create',
            name: 'tribe:create',
            component: TribeCreate,
            meta: { requiresAuth: true },
          },
        ]
      },

      {
        path: 'desa',
        meta: { requiresAuth: true },
        children: [
          {
            path: '',
            name: 'village',
            component: VillageIndex,
          },
          {
            path: ':id/edit',
            name: 'village:edit',
            component: VillageEdit,
            meta: { requiresAuth: true },
          },
          {
            path: 'create',
            name: 'village:create',
            component: VillageCreate,
            meta: { requiresAuth: true },
          },
        ]
      },

      {
        path: 'dusun',
        meta: { requiresAuth: true },
        children: [
          {
            path: '',
            name: 'hamlet',
            component: HamletIndex,
          },
          {
            path: ':id/edit',
            name: 'hamlet:edit',
            component: HamletEdit,
            meta: { requiresAuth: true },
          },
          {
            path: 'create',
            name: 'hamlet:create',
            component: HamletCreate,
            meta: { requiresAuth: true },
          },
        ]
      },

      {
        path: 'lab-item',
        meta: { requiresAuth: true },
        children: [
          {
            path: '',
            name: 'labtest',
            component: LabtestIndex,
          },
          {
            path: ':id/edit',
            name: 'labtest:edit',
            component: LabtestEdit,
            meta: { requiresAuth: true },
          },
          {
            path: 'create',
            name: 'labtest:create',
            component: LabtestCreate,
            meta: { requiresAuth: true },
          },
        ]
      },

      {
        path: 'lab-kelompok',
        meta: { requiresAuth: true },
        children: [
          {
            path: '',
            name: 'labtestgroup',
            component: LabtestgroupIndex,
          },
          {
            path: ':id/edit',
            name: 'labtestgroup:edit',
            component: LabtestgroupEdit,
            meta: { requiresAuth: true },
          },
          {
            path: 'create',
            name: 'labtestgroup:create',
            component: LabtestgroupCreate,
            meta: { requiresAuth: true },
          },
        ]
      },

      {
        path: 'tarif',
        meta: { requiresAuth: true },
        children: [
          {
            path: '',
            name: 'tariff',
            component: TariffIndex,
          },
          {
            path: ':id/edit',
            name: 'tariff:edit',
            component: TariffEdit,
            meta: { requiresAuth: true },
          },
          {
            path: 'create',
            name: 'tariff:create',
            component: TariffCreate,
            meta: { requiresAuth: true },
          },
        ]
      },

      {
        path: 'kelompok-tarif',
        meta: { requiresAuth: true },
        children: [
          {
            path: '',
            name: 'tariffgroup',
            component: TariffgroupIndex,
          },
          {
            path: ':id/edit',
            name: 'tariffgroup:edit',
            component: TariffgroupEdit,
            meta: { requiresAuth: true },
          },
          {
            path: 'create',
            name: 'tariffgroup:create',
            component: TariffgroupCreate,
            meta: { requiresAuth: true },
          },
        ]
      },

      {
        path: 'kategori-tarif',
        meta: { requiresAuth: true },
        children: [
          {
            path: '',
            name: 'tariffcategory',
            component: TariffcategoryIndex,
          },
          {
            path: ':id/edit',
            name: 'tariffcategory:edit',
            component: TariffcategoryEdit,
            meta: { requiresAuth: true },
          },
          {
            path: 'create',
            name: 'tariffcategory:create',
            component: TariffcategoryCreate,
            meta: { requiresAuth: true },
          },
        ]
      },

      {
        path: 'sumber-dana',
        meta: { requiresAuth: true },
        children: [
          {
            path: '',
            name: 'fund',
            component: FundIndex,
          },
          {
            path: ':id/edit',
            name: 'fund:edit',
            component: FundEdit,
            meta: { requiresAuth: true },
          },
          {
            path: 'create',
            name: 'fund:create',
            component: FundCreate,
            meta: { requiresAuth: true },
          },
        ]
      },
    ]
  },
] 