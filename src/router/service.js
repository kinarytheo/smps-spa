import { RegistrationIndex } from "@/views/service/registration"
import { MedicalIndex } from "@/views/service/medical"
import { CounterIndex } from "@/views/service/counter"

export default [
  {
    path: '/pelayanan',
    meta: { requiresAuth: true, layout: 'LayoutApp' },
    children: [
      {
        path: 'pendaftaran',
        name: 'registration',
        component: RegistrationIndex,
        meta: { requiresAuth: true },
      },
      {
        path: 'medis',
        name: 'medical',
        component: MedicalIndex,
        meta: { requiresAuth: true },
      }
    ]
  }
]