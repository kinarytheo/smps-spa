import { createRouter, createWebHistory } from 'vue-router'
import { useCookies } from '@vueuse/integrations/useCookies'
import MasterRoute from '@/router/master'
import HomeView from '@/views/HomeView.vue'
import Dashboard from '@/views/DashboardView.vue'
import Login from '@/views/LoginView.vue'
import notFound from '@/views/errors/NotFoundView.vue'
import PatientRoute from '@/router/patient'
import ServiceRoute from '@/router/service'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView,
      meta: { 
        layout: 'LayoutDefault' 
      },
    },
    {
      path: '/:pathMatch(.*)*',
      name: 'notfound',
      component: notFound,
      meta: { 
        layout: 'LayoutDefault' 
      },
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta: { 
        requiresAuth: false,
        layout: 'LayoutDefault' 
      },
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: Dashboard,
      meta: { 
        requiresAuth: true,
        layout: 'LayoutApp' 
      },
    },
    ...MasterRoute,
    ...PatientRoute,
    ...ServiceRoute
  ]
})

function getToken () {
  const cookies = useCookies()
  return cookies.get('simpus_token')
}

router.beforeEach((to, from, next) => {
  if (to.meta.requiresAuth && !getToken()) {
    next({ name: 'login' })
  } else if (!to.meta.requiresAuth && to.name == 'login' && getToken()) {
    next({ name: 'dashboard' }) 
  } else { 
    next() 
  }  
})

export default router
