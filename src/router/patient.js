import { PatientIndex, PatientCreate, PatientEdit, PatientShow } from "@/views/patient"

export default [
  {
    path: '/pasien',
    meta: { requiresAuth: true, layout: 'LayoutApp' },
    children: [
      {
        path: '',
        name: 'patient',
        component: PatientIndex,
      },
      {
        path: 'create',
        name: 'patient:create',
        component: PatientCreate,
        meta: { requiresAuth: true },
      },
      {
        path: ':id/edit',
        name: 'patient:edit',
        component: PatientEdit,
        meta: { requiresAuth: true },
      },
      {
        path: ':id',
        name: 'patient:show',
        component: PatientShow,
        meta: { requiresAuth: true },
      },
    ]
  },
]