import { createApp, markRaw, watch } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'
import axios from './axios'
import VueDatePicker from '@vuepic/vue-datepicker'

import './assets/main.css'
import './assets/datepicker.css'

const app = createApp(App)
const pinia = createPinia()

app.component('VueDatePicker', VueDatePicker);
app.provide('http', axios)
app.config.globalProperties.$http = axios

pinia.use(({store}) => {
  store.router = markRaw(router)
  store.http = markRaw(axios)
})

app.use(pinia)
app.use(router)

app.mount('#app')

