import PatientIndex from './PatientIndex.vue'
import PatientCreate from './PatientCreate.vue'
import PatientEdit from './PatientEdit.vue'
import PatientShow from './PatientShow.vue'

export {
  PatientIndex,
  PatientCreate,
  PatientEdit,
  PatientShow
}

