import DepartmentIndex from './DepartmentIndex.vue'
import DepartmentCreate from './DepartmentCreate.vue'
import DepartmentEdit from './DepartmentEdit.vue'

export { DepartmentIndex, DepartmentCreate, DepartmentEdit }