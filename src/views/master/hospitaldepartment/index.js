import HospitaldepartmentIndex from './HospitaldepartmentIndex.vue'
import HospitaldepartmentCreate from './HospitaldepartmentCreate.vue'
import HospitaldepartmentEdit from './HospitaldepartmentEdit.vue'

export { HospitaldepartmentIndex, HospitaldepartmentCreate, HospitaldepartmentEdit }