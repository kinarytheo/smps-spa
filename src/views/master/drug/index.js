import DrugIndex from './DrugIndex.vue'
import DrugCreate from './DrugCreate.vue'
import DrugEdit from './DrugEdit.vue'
import DrugShow from './DrugShow.vue'

export {
  DrugIndex,
  DrugCreate,
  DrugEdit,
  DrugShow
}