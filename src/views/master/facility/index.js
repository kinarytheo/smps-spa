import FacilityIndex from './FacilityIndex.vue'
import FacilityCreate from './FacilityCreate.vue'
import FacilityEdit from './FacilityEdit.vue'

export { FacilityIndex, FacilityCreate, FacilityEdit }