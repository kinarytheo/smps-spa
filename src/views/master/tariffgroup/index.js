import TariffgroupIndex from './TariffgroupIndex.vue'
import TariffgroupCreate from './TariffgroupCreate.vue'
import TariffgroupEdit from './TariffgroupEdit.vue'

export { TariffgroupIndex, TariffgroupCreate, TariffgroupEdit }