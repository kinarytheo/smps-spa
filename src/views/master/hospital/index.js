import HospitalIndex from './HospitalIndex.vue'
import HospitalCreate from './HospitalCreate.vue'
import HospitalEdit from './HospitalEdit.vue'

export { HospitalIndex, HospitalCreate, HospitalEdit }