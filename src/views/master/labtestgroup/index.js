import LabtestgroupIndex from './LabtestgroupIndex.vue'
import LabtestgroupCreate from './LabtestgroupCreate.vue'
import LabtestgroupEdit from './LabtestgroupEdit.vue'

export { LabtestgroupIndex, LabtestgroupCreate, LabtestgroupEdit }