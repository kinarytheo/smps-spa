import TribeIndex from './TribeIndex.vue'
import TribeCreate from './TribeCreate.vue'
import TribeEdit from './TribeEdit.vue'

export { TribeIndex, TribeCreate, TribeEdit }