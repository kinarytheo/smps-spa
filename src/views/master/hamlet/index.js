import HamletIndex from './HamletIndex.vue'
import HamletCreate from './HamletCreate.vue'
import HamletEdit from './HamletEdit.vue'

export { HamletIndex, HamletCreate, HamletEdit }