import PatienttypeIndex from './PatienttypeIndex.vue'
import PatienttypeCreate from './PatienttypeCreate.vue'
import PatienttypeEdit from './PatienttypeEdit.vue'

export { PatienttypeIndex, PatienttypeCreate, PatienttypeEdit }