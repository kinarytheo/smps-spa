import TariffcategoryIndex from './TariffcategoryIndex.vue'
import TariffcategoryCreate from './TariffcategoryCreate.vue'
import TariffcategoryEdit from './TariffcategoryEdit.vue'

export { TariffcategoryIndex, TariffcategoryCreate, TariffcategoryEdit }