import BloodtypeIndex from './BloodtypeIndex.vue'
import BloodtypeCreate from './BloodtypeCreate.vue'
import BloodtypeEdit from './BloodtypeEdit.vue'

export { BloodtypeIndex, BloodtypeCreate, BloodtypeEdit }