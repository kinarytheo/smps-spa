import JobIndex from './JobIndex.vue'
import JobCreate from './JobCreate.vue'
import JobEdit from './JobEdit.vue'

export { JobIndex, JobCreate, JobEdit }