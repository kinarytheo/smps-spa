import DiseaseIndex from './DiseaseIndex.vue'
import DiseaseCreate from './DiseaseCreate.vue'
import DiseaseEdit from './DiseaseEdit.vue'
import DiseaseShow from './DiseaseShow.vue'

export { DiseaseIndex, DiseaseCreate, DiseaseEdit, DiseaseShow }