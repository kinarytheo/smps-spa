import TariffIndex from './TariffIndex.vue'
import TariffCreate from './TariffCreate.vue'
import TariffEdit from './TariffEdit.vue'

export { TariffIndex, TariffCreate, TariffEdit }