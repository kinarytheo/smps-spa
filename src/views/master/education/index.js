import EducationIndex from './EducationIndex.vue'
import EducationCreate from './EducationCreate.vue'
import EducationEdit from './EducationEdit.vue'

export { EducationIndex, EducationCreate, EducationEdit }