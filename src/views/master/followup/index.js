import FollowupIndex from "./FollowupIndex.vue"
import FollowupCreate from "./FollowupCreate.vue"
import FollowupEdit from "./FollowupEdit.vue"

export { FollowupIndex, FollowupCreate, FollowupEdit }