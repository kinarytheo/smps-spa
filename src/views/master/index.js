import MasterView from './MasterView.vue'

export { MasterView }
export * from './agegroup'
export * from './bloodtype'
export * from './department'
export * from './disease'
export * from './diseasegroup'
export * from './drug'
export * from './education'
export * from './facility'
export * from './followup'
export * from './fund'
export * from './hamlet'
export * from './hospital'
export * from './hospitaldepartment'
export * from './job'
export * from './labtest'
export * from './labtestgroup'
export * from './patienttype'
export * from './tariff'
export * from './tariffcategory'
export * from './tariffgroup'
export * from './tribe'
export * from './village'
