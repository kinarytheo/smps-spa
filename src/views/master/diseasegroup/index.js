import DiseasegroupIndex from './DiseasegroupIndex.vue'
import DiseasegroupCreate from './DiseasegroupCreate.vue'
import DiseasegroupEdit from './DiseasegroupEdit.vue'

export {
  DiseasegroupIndex,
  DiseasegroupCreate,
  DiseasegroupEdit
}