import VillageIndex from './VillageIndex.vue'
import VillageCreate from './VillageCreate.vue'
import VillageEdit from './VillageEdit.vue'

export { VillageIndex, VillageCreate, VillageEdit }