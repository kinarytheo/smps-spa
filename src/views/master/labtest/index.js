import LabtestIndex from './LabtestIndex.vue'
import LabtestCreate from './LabtestCreate.vue'
import LabtestEdit from './LabtestEdit.vue'

export { LabtestIndex, LabtestCreate, LabtestEdit }