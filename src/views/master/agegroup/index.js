import AgegroupIndex from './AgegroupIndex.vue'
import AgegroupCreate from './AgegroupCreate.vue'
import AgegroupEdit from './AgegroupEdit.vue'

export { AgegroupIndex, AgegroupCreate, AgegroupEdit }